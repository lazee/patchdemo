package com.example.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class Patcher {

    @JsonProperty
    private Map<String, String> props;

    @JsonCreator
    public Patcher(@JsonProperty Map<String, String> props) {
        this.props = props;
    }

    public Map<String, String> getProps() {
        return props;
    }

    public void setProps(Map<String, String> props) {
        this.props = props;
    }

    @Override
    public String toString() {
        if (props == null) {
            return "Patcher{" +
                    "props=null" +
                    '}';
        }

        StringBuffer sb = new StringBuffer();
        sb.append("Patcher{");
        for (String key : props.keySet()) {
            sb.append(key + ":" + props.get(key) + ";   ");
        }
        sb.append("}");
        return sb.toString();
    }
}
