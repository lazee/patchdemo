package com.example.web;

import com.example.domain.Patcher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/")
public class MyRestController {

    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<String> patchUser(@PathVariable String id, @RequestBody Patcher body) {

        return new ResponseEntity<>("Wooba Booba !" + body.toString(), HttpStatus.OK);
    }
}
